
/*
using System;

namespace FancyCalc
{
    public class FancyCalcEnguine
    {

        public double Add(int a, int b)
        {
            throw new NotImplementedException();
            //return a + b;
        }


        public double Subtract(int a, int b)
        {
            throw new NotImplementedException();
           // return a - b;
        }


        public double Multiply(int a, int b)
        {
            //return a * b;
             throw new NotImplementedException();
            
        }

        //generic calc method. usage: "10 + 20"  => result 30
        public double Culculate(string expression)
        {
            throw new NotImplementedException();
           // return 6;

        }
    }
}
*/


using System;

namespace Arrays
{
    public enum SortOrder { Ascending, Descending };
    static public class OperationsWithArrays
    {
        public static void SortArray(int[] array, SortOrder order)
        {
            int tmp;
            int i, j;

            //if (array == null)
            //    throw new ArgumentNullException();
            //else
            {
                for (i = 0; i < array.Length - 1; i++)
                    for (j = 0; j < array.Length - 1 - i; j++)
                    {

                        if ((order == SortOrder.Ascending) && (array[j] > array[j + 1]) ||
                            (order == SortOrder.Descending) && (array[j] < array[j + 1]))
                        {
                            swap();
                        }
                    }

                void swap()
                {
                    tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        public static bool IsSorted(int[] array, SortOrder order)
        {
            //UNCOMMENT THREE LINES BELOW
            //if (array == null)
            //    throw new ArgumentNullException();
            //else
            {
                if (order == SortOrder.Ascending)
                {
                    for (int i = 0; i < array.Length - 1; i++)
                    {
                        if (array[i] > array[i + 1])
                            return false;
                    }
                    return true;
                }
                else
                {
                    for (int i = 0; i < array.Length - 1; i++)
                    {
                        if (array[i] < array[i + 1])
                            return false;
                    }
                    return true;
                }
            }
        }
    }
}
